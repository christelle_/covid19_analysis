import plotly.graph_objects as go
import plotly.express as px
from datetime import datetime


def get_map(map_data, contours, indicator='P_rate', indicator_lab='Pösitivity rate', date='2020-05-13', selection=None):

    # MAP
    # create trace
    filtered = map_data[map_data.jour == date]
    colorscale = 'Reds' if not indicator.startswith('vaccine') else 'Greens'
    fig = px.choropleth_mapbox(filtered, geojson=contours, color=indicator,
                               locations='reg', featureidkey='properties.new_code',
                               hover_name='nom_région',
                               color_continuous_scale=colorscale, opacity=0.5)

    # highlight selection
    selections = []
    if (selection is not None) & (filtered.shape[0] != 0):
        selections = [point['location'] for point in selection['points']]
        regions_lookup = {int(feature['properties']['new_code']): feature for feature in contours['features']}
        contours_highlights = get_highlights(selections, geojson=contours, lookup=regions_lookup)
        fig.add_trace(
            px.choropleth_mapbox(filtered, geojson=contours_highlights, color=indicator,
                                 locations='reg', featureidkey='properties.new_code', hover_name='reg',
                                 color_continuous_scale="Reds", opacity=1).data[0]
        )

    # layout
    fig.update_layout(
            title_text=f'{indicator_lab} - {date}',
            mapbox_style="carto-positron",
            mapbox={'center': go.layout.mapbox.Center(lat=47.007533, lon=3.410235), 'zoom': 4},
            showlegend=True,
            geo=dict(
                scope='europe',
            ),
            margin={"r": 50, "t": 50, "l": 0, "b": 0, "autoexpand": False},
            # width=450,
            # height=550
        )
    fig['layout']['uirevision'] = 'some-constant'

    fig.update_geos(
        showcoastlines=True, coastlinecolor="#091957",
        showland=True, landcolor="#ccd2d1",
        showsubunits=True,
        showocean=True, oceancolor="#499fa9"
    )
    return fig


def get_lineplot_ovw(incidence_reg, incidence_fr, indicator='P_nominal', selection=None):

    # TIME SERIES
    fig2 = go.Figure()
    lockdowns = ['2020-03-17', '2020-10-30']
    if selection:
        filtered_reg = incidence_reg.dropna(subset=[indicator])
        selections = [point['location'] for point in selection['points']]
        lineplot_data = filtered_reg[filtered_reg['reg'].isin(selections)]
        fig2 = px.line(lineplot_data, x='jour', y=indicator, color='reg')
        # fig2.add_trace(go.Scatter(x=lineplot_data['jour'], y=lineplot_data[indicator],
        #                          mode='lines+markers',
        #                          name=indicator))
        # add events
        # fig2.add_trace(go.Bar(x=['2020-03-17', '2020-10-30'],
        #                       y=[max(lineplot_data[indicator]), max(lineplot_data[indicator])],
        #                       name='lockdowns',
        #                       showlegend=False,
        #                       customdata=['1st lockdown', '2nd lockdown'],
        #                       width=0.2,  # bar width
        #                       hovertemplate='date: %{x}<br>event: %{customdata}',
        #                       opacity=0.65
        #                       ), secondary_y=True)
    else:
        filtered_fr = incidence_fr.dropna(subset=[indicator])
        # fig2.add_trace(go.Scatter(x=filtered_fr['jour'], y=filtered_fr[indicator],
        #                           mode='lines+markers',
        #                           name=indicator))
        fig2 = px.line(filtered_fr, x='jour', y=indicator)
    fig2.update_layout(
        title_text=f'Overall evolution',
        xaxis=dict(
            showline=True,
            showgrid=True,
            showticklabels=True,
            linecolor='rgb(204, 204, 204)',
            linewidth=4,
            ticks='outside',
            tickfont=dict(
                family='Arial',
                size=12,
                color='rgb(82, 82, 82)',
            ),
        ),
        yaxis=dict(
            showgrid=True,
            zeroline=True,
            showline=True,
            showticklabels=True,
        ),
        autosize=False,
        margin=dict(
            autoexpand=True,
            l=0,
            r=0,
            t=50
        ),
        # width=580,
        # height=550,
        showlegend=False,
    )
    return fig2


def get_lineplot_fcst(fcst_data, indicator='total_cas_confirmes', exo_vars_str=None):

    # TIME SERIES
    filtered = fcst_data.dropna(subset=[indicator])
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=filtered['date'], y=filtered[indicator],
                             mode='lines+markers',
                             name=indicator))
    fig.add_trace(go.Scatter(x=filtered['date'], y=filtered['%s_pred_%s' % (indicator, exo_vars_str)],
                             mode='lines+markers',
                             name='forecast'))
    # fig = px.line(filtered_fr, x='date', y=indicator)
    fig.update_layout(
        xaxis=dict(
            showline=True,
            showgrid=True,
            showticklabels=True,
            linecolor='rgb(204, 204, 204)',
            linewidth=4,
            ticks='outside',
            tickfont=dict(
                family='Arial',
                size=12,
                color='rgb(82, 82, 82)',
            ),
        ),
        yaxis=dict(
            showgrid=True,
            zeroline=True,
            showline=True,
            showticklabels=True,
        ),
        autosize=False,
        margin=dict(
            autoexpand=True,
            l=0,
            r=0,
            t=50
        ),
        # width=1000,
        # height=550,
        showlegend=False,
    )
    return fig


def get_highlights(selections, geojson=None, lookup=None):
    geojson_highlights = dict()
    for k in geojson.keys():
        if k != 'features':
            geojson_highlights[k] = geojson[k]
        else:
            geojson_highlights[k] = [lookup[selection] for selection in selections]
    return geojson_highlights