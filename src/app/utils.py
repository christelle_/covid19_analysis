from matplotlib import pyplot as plt
from datetime import datetime, timedelta
import pandas as pd
import pmdarima as pm


exo_def = dict(days_after_lockdown=['days_after_lockdown'],
               r_1_lag=['r_lag_1'],
               r_7_lag=['r_lag_%s' % lag for lag in range(1, 8)],
               r_15_lag=['r_lag_%s' % lag for lag in range(1, 16)],
               days_after_lockdown_r_1_lag=['days_after_lockdown'] + ['r_lag_1'],
               days_after_lockdown_r_7_lag=['days_after_lockdown'] + ['r_lag_%s' % lag for lag in range(1, 8)],
               days_after_lockdown_r_15_lag=['days_after_lockdown'] + ['r_lag_%s' % lag for lag in range(1, 16)],
               no_exo=[])


def get_fcst_graphs_data(synthesis, indicators, exo_def=None, series=None):
    # merge datasets
    extract_r0 = indicators[['extract_date', 'R']].rename(columns={'extract_date': 'date'})
    fcst_data = pd.merge(synthesis, extract_r0, how='inner', on=['date'])
    fcst_data['date'] = fcst_data['date'].map(lambda date: datetime.strptime(date, '%Y-%m-%d'))

    # sort dataframe
    fcst_data = fcst_data.sort_values(by='date').reset_index(drop=True)

    # remove some series
    # fcst_data = fcst_data.drop(columns=['total_cas_possibles_ehpad', 'total_patients_gueris',
    #                                     'nouveaux_patients_hospitalises'], axis=1)

    # add lockdown dates & delays
    fcst_data['days_after_lockdown'] = fcst_data['date'].map(compute_days_after_lock)

    # add lagged series for R0 over 2 weeks
    for lag in range(1, 16):
        fcst_data['r_lag_%d' % lag] = fcst_data['R'].shift(periods=lag)

    # add new confirmed cases
    fcst_data['nouveaux_cas_confirmes'] = fcst_data['total_cas_confirmes'] - fcst_data['total_cas_confirmes'].shift(
        periods=1)

    # forecast ts
    if exo_def is None:
        exo_def = dict(no_exo=[])
    if series:
        for ts in series:
            seasonal = False if ts == 'total_cas_confirmes' else True
            for exo_type, exo in exo_def.items():
                # exo_type = '_'.join(sorted(exo)) if exo else 'no_exo'
                cols = ['date', ts] + exo
                data = fcst_data[cols].dropna()
                res = forecast_ts(data, ts, exo, exo_type, seasonal=seasonal)
                fcst_data = pd.concat([fcst_data, res], axis=1)
    else:
        raise ValueError('You must provide a list of column names to be forecasted.')
    return fcst_data


def forecast_ts(fcst_data, ts, exo, exo_type, seasonal=False, plot_preds=False):
    print('ts: %s' % ts)
    print('\texo type: %s' % exo_type)
    ts_serie = fcst_data[ts]
    exo_serie = fcst_data[exo] if exo else None
    model = pm.auto_arima(ts_serie, start_p=1, start_q=1, exogenous=exo_serie,
                          test='adf',  # use adftest to find optimal 'd'
                          max_p=3, max_q=3,  # maximum p and q
                          d=None,  # let model determine 'd'
                          seasonal=seasonal,  # No Seasonality
                          start_P=0,
                          D=0,
                          trace=True,
                          #                       error_action='ignore',
                          #                       suppress_warnings=True,
                          stepwise=True)

    # save summary
    plt.rc('figure', figsize=(12, 7))
    plt.margins(0, 0)
    plt.text(0.01, 0.05, str(model.summary()), {'fontsize': 10}, fontproperties='monospace', ha='center', va='center')
    plt.axis('off')
    plt.tight_layout()
    plt.savefig('/app/app/assets/summary_%s_%s.png' % (ts, exo_type), bbox_inches='tight', pad_inches=0)
    # plt.savefig('./assets/summary_%s_%s.png' % (ts, exo_type), bbox_inches='tight', pad_inches=0)
    plt.clf()

    # residuals diagnostics
    model.plot_diagnostics(figsize=(15, 10))
    plt.savefig('/app/app/assets/residuals_%s_%s.png' % (ts, exo_type), bbox_inches='tight', pad_inches=0)
    # plt.savefig('./assets/residuals_%s_%s.png' % (ts, exo_type), bbox_inches='tight', pad_inches=0)
    plt.clf()

    # train_test split
    today = datetime.strptime(datetime.now().strftime('%Y-%m-%d'), '%Y-%m-%d')
    first_training = datetime.strptime('2021-02-05', '%Y-%m-%d')
    first_split_date = datetime.strptime('2021-01-01', '%Y-%m-%d')
    split_date = first_split_date + timedelta(days=(today - first_training).days)
    train = fcst_data[fcst_data.date < split_date]
    test = fcst_data[fcst_data.date > split_date]
    y_train = train[[ts]].values
    X_train = train[exo].values if exo else None
    y_test = test[[ts]].values
    X_test = test[exo].values if exo else None
    prediction_period = y_test.shape[0]

    # fit and predict
    model.fit(y=y_train, X=X_train)
    forecast = model.predict(n_periods=prediction_period, X=X_test, return_conf_int=True)

    forecast_df = pd.DataFrame(forecast[0], index=test.index, columns=['%s_pred_%s' % (ts, exo_type)])
    cb_lower = pd.DataFrame(forecast[1][:, 0], index=test.index, columns=['%s_pred_cb_lower_%s' % (ts, exo_type)])
    cb_upper = pd.DataFrame(forecast[1][:, 1], index=test.index, columns=['%s_pred_cb_upper_%s' % (ts, exo_type)])
    forecast_df = pd.concat([test, forecast_df, cb_lower, cb_upper], axis=1)

    # plot predictions
    if plot_preds:
        ax, fig = plt.subplots(figsize=(20, 10))
        plt.plot(fcst_data['date'], fcst_data[ts], label='actual')
        plt.plot(forecast_df['date'], forecast_df['%s_pred_%s' % (ts, exo_type)], label='forecast')
        plt.fill_between(forecast_df['date'],
                         forecast_df['%s_pred_cb_lower_%s' % (ts, exo_type)],
                         forecast_df['%s_pred_cb_upper_%s' % (ts, exo_type)],
                         color='k', alpha=0.1)
        plt.title('%s forecast' % ts)
        plt.legend(loc='upper left', fontsize=8)
        plt.show()

    # concatenate with train
    train['%s_pred_%s' % (ts, exo_type)] = None
    train['%s_pred_cb_lower_%s' % (ts, exo_type)] = None
    train['%s_pred_cb_upper_%s' % (ts, exo_type)] = None
    forecast_df = pd.concat([train, forecast_df], axis=0).sort_values(by='date').reset_index(drop=True)

    return forecast_df[['%s_pred_%s' % (ts, exo_type),
                        '%s_pred_cb_lower_%s' % (ts, exo_type),
                        '%s_pred_cb_upper_%s' % (ts, exo_type)]]


def compute_days_after_lock(date):
    lockdowns = [datetime.strptime('2020-03-17', '%Y-%m-%d'), datetime.strptime('2020-10-30', '%Y-%m-%d')]
    deltas = [(date - lock).days for lock in lockdowns if date > lock]
    return min(deltas)