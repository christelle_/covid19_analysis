from app.app_ import covid19_app
from app.utils import exo_def, get_fcst_graphs_data
from datetime import datetime
import pandas as pd
import json


def get_ovw_graphs_data(incidence_reg, incidence_fr, incidence_rea_reg_cl_age, regions_metadata, vaccined_reg):
    incidence_reg = incidence_reg.drop(columns=['cl_age90']).groupby(['jour', 'reg']).sum().reset_index()
    incidence_fr = incidence_fr.drop(columns=['fra']).groupby(['jour']).sum().reset_index()

    incidence_reg = incidence_reg.rename(columns={'P': 'P_nominal'})
    incidence_reg['P_rate'] = 100000 * incidence_reg['P_nominal'] / incidence_reg['pop']
    incidence_fr = incidence_fr.rename(columns={'P': 'P_nominal'})
    incidence_fr['P_rate'] = 100000 * incidence_reg['P_nominal'] / incidence_reg['pop']

    incidence_rea_reg_cl_age = incidence_rea_reg_cl_age.rename(columns={'rea': 'rea_nominal'})
    incidence_rea_reg = incidence_rea_reg_cl_age.drop(columns=['cl_age90']).groupby(['jour', 'reg']).sum().reset_index()
    incidence_rea_fr = incidence_rea_reg_cl_age.drop(columns=['cl_age90']).groupby(['jour']).sum().reset_index()

    # join reg data
    incidence_reg = pd.merge(incidence_reg, incidence_rea_reg, how='inner', on=['jour', 'reg'])
    incidence_fr = pd.merge(incidence_fr, incidence_rea_fr, how='inner', on=['jour'])

    # vaccinations
    vaccined_reg = vaccined_reg.rename(columns={'date': 'jour', 'total_vaccines': 'vaccine_nominal'})
    vaccined_fr = vaccined_reg.drop(columns=['code', 'nom']).groupby(['jour']).sum().reset_index()
    vaccined_reg['reg'] = vaccined_reg['code'].map(lambda code: code.split('-')[1]).astype('int')

    # add vaccination to consolidated data
    region_data = pd.merge(incidence_reg, vaccined_reg, how='outer', on=['jour', 'reg'])
    fr_data = pd.merge(incidence_fr, vaccined_fr, how='outer', on=['jour'])

    # add regions metadata
    regions_metadata = regions_metadata.rename(columns={'code_région': 'reg'})
    region_data = pd.merge(region_data, regions_metadata[['nom_région', 'reg', 'pop_region']], how='inner',
                             on='reg')
    region_data['vaccine_nominal'] = region_data['vaccine_nominal'].fillna(0.)
    region_data['vaccine_rate'] = region_data['vaccine_nominal'] / region_data['pop_region']
    fr_data['vaccine_rate'] = fr_data['vaccine_nominal'] / 67.060*1e6
    return region_data, fr_data


incidence_reg = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/incidence_region.csv', sep=';')
incidence_fr = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/incidence_fr.csv', sep=';')
incidence_rea_reg_cl_age = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/donnees-hospitalieres-classe-age-covid19.csv', sep=';')
synthesis = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/synthese-fra.csv', sep=',')
regions_metadata = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/centre_regions.csv', sep=';')
vaccined_reg = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/vaccination-regional.csv', sep=',')
indicators = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/indicators.csv', sep=',')
with open('d:/Users/cvignon/Documents/ONDA/data/contours-geographiques-des-nouvelles-regions-metropole.geojson') as f:
    contours = json.load(f)

# overview
region_data, fr_data = get_ovw_graphs_data(incidence_reg, incidence_fr, incidence_rea_reg_cl_age, regions_metadata,
                                              vaccined_reg)
# forecasts
# fcst_data = get_fcst_graphs_data(synthesis, indicators,
#                                  exo_def=exo_def, series=['nouveaux_cas_confirmes', 'nouveaux_patients_reanimation'])
# fcst_data.to_csv('d:/Users/cvignon/Documents/ONDA/data/forecast_data.csv')
fcst_data = pd.read_csv('d:/Users/cvignon/Documents/ONDA/data/forecast_data.csv')

# run app
app = covid19_app(region_data, fr_data, contours, fcst_data)
app.run_server(debug=True, port=8051, host='127.0.0.1')

# import requests
# import pandas as pd
# import io
#
# dat = requests.get("https://www.data.gouv.fr/fr/datasets/r/08c18e08-6780-452d-9b8c-ae244ad529b3")
# df = pd.read_csv(io.StringIO(dat.text), sep=';')
# print(df)