from app.source import incidence_rate_reg, incidence_rate_fr, incidence_intensive_care_reg, synthesis_fr, geo_contours,\
    regions_metadata, vaccined_regional, pandemic_indicators
from app.utils import exo_def, get_fcst_graphs_data
from app.app_ import covid19_app
from onda.pipeline.job import Output, Input
from onda.pipeline.job import pipeline_job
from onda.storage.base import StorageService
import pandas as pd
import json
import os
from datetime import datetime
BUCKET_NAME = os.getenv('BUCKET_NAME', None)


@pipeline_job(reg_metadata=Output(f'/{regions_metadata["logical_path"]}'),
              geo_contours=Output(f'/{geo_contours["logical_path"]}'))
def create_meta(reg_metadata, geo_contours):
    # service = StorageService.factory([f's3://{BUCKET_NAME}'])
    # service.write_to_s3(filetype='upload',
    #                     filename='./metadata/contours-geographiques-des-nouvelles-regions-metropole.geojson')
    # service.write_to_s3(filetype='upload',
    #                     filename='./metadata/centre_regions.csv')
    reg_meta = pd.read_csv('/app/app/metadata/centre_regions.csv', sep=';').reset_index(drop=True)
    reg_metadata.write_dataframe(reg_meta)
    with open('/app/app/metadata/contours-geographiques-des-nouvelles-regions-metropole.geojson', 'r') as f:
        contours = json.load(f)
    geo_contours.write_dataframe(contours, filetype='json')


@pipeline_job(incidence_reg=Input(f'/{incidence_rate_reg["logical_path"]}'),
              incidence_fr=Input(f'/{incidence_rate_fr["logical_path"]}'),
              incidence_rea_reg_cl_age=Input(f'/{incidence_intensive_care_reg["logical_path"]}'),
              vaccined_reg=Input(f'/{vaccined_regional["logical_path"]}'),
              reg_metadata=Input(f'/{regions_metadata["logical_path"]}'),
              consolidation_reg=Output('/covid19/france/consolidation_region.csv'),
              consolidation_fr=Output('/covid19/france/consolidation_fr.csv'))
def get_graphs_data(incidence_reg, incidence_fr, incidence_rea_reg_cl_age, vaccined_reg, reg_metadata,
                    consolidation_reg, consolidation_fr, job):
    # get dataframes
    incidence_reg = incidence_reg.read_dataframe(sep=';')
    incidence_fr = incidence_fr.read_dataframe(sep=';')
    incidence_rea_reg_cl_age = incidence_rea_reg_cl_age.read_dataframe(sep=';')
    vaccined_reg = vaccined_reg.read_dataframe(sep=',')
    regions_metadata = reg_metadata.read_dataframe(sep=',')

    # transform incidence data
    incidence_reg = incidence_reg.drop(columns=['cl_age90']).groupby(['jour', 'reg']).sum().reset_index()
    incidence_fr = incidence_fr.drop(columns=['fra']).groupby(['jour']).sum().reset_index()

    incidence_reg = incidence_reg.rename(columns={'P': 'P_nominal'})
    incidence_reg['P_rate'] = 100000 * incidence_reg['P_nominal'] / incidence_reg['pop']
    incidence_fr = incidence_fr.rename(columns={'P': 'P_nominal'})
    incidence_fr['P_rate'] = 100000 * incidence_reg['P_nominal'] / incidence_reg['pop']

    incidence_rea_reg_cl_age = incidence_rea_reg_cl_age.rename(columns={'rea': 'rea_nominal'})
    incidence_rea_reg = incidence_rea_reg_cl_age.drop(columns=['cl_age90']).groupby(['jour', 'reg']).sum().reset_index()
    incidence_rea_fr = incidence_rea_reg_cl_age.drop(columns=['cl_age90']).groupby(['jour']).sum().reset_index()

    # join reg data
    incidence_reg = pd.merge(incidence_reg, incidence_rea_reg, how='inner', on=['jour', 'reg'])
    incidence_fr = pd.merge(incidence_fr, incidence_rea_fr, how='inner', on=['jour'])

    # vaccinations
    vaccined_reg = vaccined_reg.rename(columns={'date': 'jour', 'total_vaccines': 'vaccine_nominal'})
    vaccined_fr = vaccined_reg.drop(columns=['code', 'nom']).groupby(['jour']).sum().reset_index()
    vaccined_reg['reg'] = vaccined_reg['code'].map(lambda code: code.split('-')[1]).astype('int')

    # add vaccination to consolidated data
    region_data = pd.merge(incidence_reg, vaccined_reg, how='outer', on=['jour', 'reg'])
    fr_data = pd.merge(incidence_fr, vaccined_fr, how='outer', on=['jour'])

    # add regions metadata
    regions_metadata = regions_metadata.rename(columns={'code_région': 'reg'})
    region_data = pd.merge(region_data, regions_metadata[['nom_région', 'reg', 'pop_region']], how='inner',
                           on='reg')
    region_data['vaccine_nominal'] = region_data['vaccine_nominal'].fillna(0.)
    region_data['vaccine_rate'] = region_data['vaccine_nominal'] / region_data['pop_region']
    fr_data['vaccine_rate'] = fr_data['vaccine_nominal'] / 67.060 * 1e6

    # write_dataframes
    consolidation_reg.write_dataframe(region_data)
    consolidation_fr.write_dataframe(incidence_fr)


@pipeline_job(synthesis=Input(f'/{synthesis_fr["logical_path"]}'),
              indicators=Input(f'/{pandemic_indicators["logical_path"]}'),
              forecast=Output('/covid19/france/forecasts.csv'))
def get_fcst_data(synthesis, indicators, forecast, job):
    synthesis_ = synthesis.read_dataframe(sep=',')
    indicators_ = indicators.read_dataframe(sep=',')

    series = ['nouveaux_cas_confirmes', 'nouveaux_patients_reanimation']
    fcst_data = get_fcst_graphs_data(synthesis_, indicators_, exo_def=exo_def, series=series)

    # write output
    forecast.write_dataframe(fcst_data)


@pipeline_job(consolidation_reg=Input('/covid19/france/consolidation_region.csv'),
              consolidation_fr=Input('/covid19/france/consolidation_fr.csv'),
              contours=Input(f'/{geo_contours["logical_path"]}'),
              forecasts=Input('/covid19/france/forecasts.csv'),
              silly_out=Output("/test_dash_app"))
def covid_app(consolidation_reg, consolidation_fr, contours, forecasts, silly_out, job):
    cons_reg = consolidation_reg.read_dataframe(sep=',').dropna()
    cons_fr = consolidation_fr.read_dataframe(sep=',')
    forecasts_ = forecasts.read_dataframe(sep=',')
    contours_ = contours.read_dataframe(filetype='json')[0]

    # run app
    silly_out.write_dataframe(pd.DataFrame())
    app = covid19_app(cons_reg, cons_fr, contours_, forecasts_)
    app.run_server(debug=False, port=8051, host='0.0.0.0')