from app.figures import get_map, get_lineplot_ovw, get_lineplot_fcst
import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
from dash.dependencies import Input, Output
from dash_table import DataTable
from datetime import datetime, timedelta


def covid19_app(region_data, fr_data, contours, fcst_data):
    covid_indicators = {'Confirmed cases': 'total_cas_confirmes',
                        'EHPAD confirmed cases': 'total_cas_confirmes_ehpad',
                        'EHPAD possible cases': 'total_cas_possibles_ehpad',
                        'Hospital Deaths': 'total_deces_hopital', 'EHPAD deaths': 'total_deces_ehpad',
                        'Intensive care': 'patients_reanimation',
                        'Admissions': 'patients_hospitalises',
                        'New intensive care patients': 'nouveaux_patients_reanimation'}
    other_indicators = {'P': 'Positivity', 'rea': 'Intensive care Load', 'vaccine': 'Vaccination'}
    other_indicators_type = {'nominal': 'Nominal', 'rate': 'Rate'}

    fcst_data['date'] = fcst_data['date'].map(lambda date: datetime.strptime(date, '%Y-%m-%d'))

    # APP
    app = dash.Dash(__name__,
                    meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
                    external_stylesheets=[dbc.themes.FLATLY])
    app.config['suppress_callback_exceptions'] = True

    # header
    header = dbc.Navbar([

            html.A(
                [
                    dbc.Row(
                        [
                            dbc.Col(html.Img(src=app.get_asset_url('virus_logo_.png'), height='45px'),
                                    className='ml-5',
                                    width=1),
                            dbc.Col(dbc.NavbarBrand('COVID19 Explorer',
                                                    style=dict(color='#FFFFFF', fontSize=20))),
                            dbc.Col(dbc.Nav(
                                [
                                    dbc.NavLink("Overview", href="/ovw", active=True,
                                                        style={'color': '#FFFFFF'}),
                                    dbc.NavLink("Forecasts", href="/forecast", active="exact",
                                                        style={'color': '#FFFFFF'})
                                ],
                                vertical=False,
                                pills=True,
                            ),
                                width=3,
                                className='ml-5'),
                            dbc.Col(html.Img(src=app.get_asset_url('cs_logo.png'), height='30px'),
                                    width=1,
                                    className='ml-auto')
                        ],
                        align='center',
                        no_gutters=True,
                        className='d-flex flex-row'
                    )
            ],
            style={'width': '100%'}
            )
    ], className='nav-pills bg-primary', color='#3f4954')
    content = html.Div(id="page-content")

    # overview
    overview = dbc.Container(children=[
        dbc.Row([
            dbc.Col([
                html.Div([
                    dbc.Row([
                        dbc.Col(html.B('Choose an indicator:', style={'padding-left': '5vw'}), width=3),
                        dbc.Col(dcc.RadioItems(id='radio_choice',
                                               options=[dict(label=lab, value=lab_col)
                                                        for lab_col, lab in other_indicators.items()],
                                               value='P',
                                               style={'margin-top': '1vh'},
                                               labelStyle={'display': 'block'}
                                               ),
                                className='d-flex'
                                ),
                        dbc.Col(html.B('Choose a date:'), width=2),
                        dbc.Col(dcc.DatePickerSingle(id='date_picker',
                                                     min_date_allowed=datetime.strptime('2020-05-13', '%Y-%m-%d'),
                                                     max_date_allowed=datetime.now(),
                                                     initial_visible_month=datetime.now(),
                                                     date=datetime.now(),
                                                     style={'margin-top': '1vh'}),
                                className='d-flex')
                    ],
                        className='d-flex',
                        no_gutters=True,
                        align='center'
                    )
                ],
                    style={'display': 'inline-block', 'width': '100%', 'background-color': '#ffffff'}),
                html.Hr(className='d-flex my-1'),
                html.Div(dcc.RadioItems(id='radio_choice_plot',
                                        options=[dict(label=lab, value=lab_col)
                                                 for lab_col, lab in other_indicators_type.items()],
                                        value='nominal',
                                        # labelStyle={'display': 'inline-block'}
                                        ),
                         style={'display': 'flex', 'align-items': 'center', 'justify-content': 'center'},
                         className='d-flex'),
                html.Div([
                    dbc.Row(
                        [
                            dbc.Col(
                                dcc.Graph(id='map', style={'width': '100%', 'height': '80%',
                                                           'margin-left': '5vw', 'margin-top': '1vh'}),
                                width=5,
                                className='d-flex'),
                            dbc.Col([
                                dcc.Graph(id='lineplot', style={'display': 'block',
                                                                'width': '100%',
                                                                'margin-top': '1vh'})
                            ],
                                width=dict(size=7, offset=0),
                                className='d-flex'),
                        ],
                        className='d-flex',
                        no_gutters=True,
                        style={'display': 'inline-block'}
                    ),
                    dbc.Card(id='indicator-table-card', children=[
                        dbc.CardHeader(id='table-header', style={'background-color': '#e6e8e8'}),
                        dbc.CardBody(
                            DataTable(id='indicator-table',
                                      style_cell_conditional=[
                                          {'if': {'column_id': c}, 'textAlign': 'left'}
                                          for c in ['Region Code', 'Region']
                                      ],
                                      style_header={
                                          'backgroundColor': '#348787',
                                          'fontWeight': 'bold',
                                          'color': '#ffffff'
                                      },
                                      sort_action='native'
                            )
                        )
                    ])
                ],
                    style={'display': 'inline-block', 'height': '100%', 'width': '100%',
                           'maxHeight': '100vh', 'overflow': 'scroll'}
                )
            ],
                width=9),
            dbc.Col(
                dbc.Container(
                    id='sidebar',
                    children=[
                        html.B('More daily indicators', className='d-flex display-6', style={'fontSize': 25}),
                        dbc.CardColumns(id='ind_cards', style={'margin-top': '5vh'})
                        ],
                    style={'display': 'inline-block', 'width': '100%', 'height': '100%'}
                ),
                className='d-flex jumbotron ml-auto',
                width=3
            )
        ],
            className='d-flex h-100 w-100',
            no_gutters=True,
            style={'position': 'absolute'}
        )

    ], style={'height': '100%', 'width': '100%', 'margin': 0, 'padding': 0}, className='d-flex')

    # forecasts
    methods = dict(arima='ARIMA Model', markov='Hidden Markov Model')
    exo_variables = dict(days_after_lockdown='No. days after last lockdown',
                         r_1_lag='R0 lag (1 day)', r_7_lag='R0 lag (1 week)', r_15_lag='R0 lag (2 weeks)')
    series_to_forecast = dict(nouveaux_cas_confirmes='New confirmed cases',
                              nouveaux_patients_reanimation='New intensive care patients')
    forecasts = dbc.Container(children=[
            dbc.Row([
                dbc.Col([
                    dbc.Row([
                        dbc.Col(html.B('Choose forecast method:', style={'padding-left': '5vw'}),
                                width=4,
                                #className='d-flex'
                                ),
                        dbc.Col(dcc.RadioItems(id='model_choice',
                                               options=[dict(label=lab, value=meth, disabled=(meth == 'markov'))
                                                        for meth, lab in methods.items()],
                                               value='arima',
                                               style={'margin-top': '4vh'},
                                               labelStyle={'display': 'block'}
                                               ),
                                #className='d-flex'
                                )
                    ],
                        className='d-flex',
                        no_gutters=True,
                        align='center'
                    ),
                    html.Hr(className='d-flex my-1'),
                    dbc.Row([
                        dbc.Container(id='forecast_cards',
                                      style={'width': '100%', 'align-items': 'center',
                                             'maxHeight': '100vh', 'overflow': 'scroll'})
                    ],
                        className='d-flex',
                        no_gutters=True,
                        align='center'
                    ),
                ],
                    #className='d-flex',
                    width=9),
                dbc.Col(
                    dbc.Container(
                        id='sidebar_fcst',
                        children=[
                            html.B('Exogenous variables', className='d-flex display-6', style={'fontSize': 25}),
                            dcc.Checklist(id='exo-vars',
                                          options=[{'label': lab, 'value': val, 'disabled': False}
                                                   for val, lab in exo_variables.items()],
                                          value=[],
                                          labelStyle={'display': 'block'},
                                          style={'margin-top': '5vh'})
                        ],
                        style={'display': 'inline-block', 'width': '100%', 'height': '100%'}
                    ),
                    className='d-flex jumbotron ml-auto',
                    width=3
                )
            ],
                className='d-flex h-100 w-100',
                no_gutters=True,
                style={'position': 'absolute'}
            )

    ], style={'height': '100%', 'width': '100%', 'margin': 0, 'padding': 0}, className='d-flex')


    # CALLBACKS
    @app.callback(Output("page-content", "children"), [Input("url", "pathname")])
    def render_page_content(pathname):
        if pathname in ["/ovw", "/", ""]:
            return html.Div([overview])
        elif pathname == "/forecast":
            return forecasts
        # If the user tries to reach a different page, return a 404 message
        return dbc.Jumbotron(
            [
                html.H1("404: Not found", className="text-danger"),
                html.Hr(),
                html.P(f"The pathname {pathname} was not recognised..."),
            ])

    @app.callback(Output('map', 'figure'),
                  Input('radio_choice', 'value'),
                  Input('radio_choice_plot', 'value'),
                  Input('date_picker', 'date'),
                  Input('map', 'clickData'))
    def update_map(ind, ind_type, date, map_selection):
        """Update map on indicator selection"""
        date = date.split('T')[0]
        ind_lab = f'{other_indicators[ind]} ({other_indicators_type[ind_type]})'
        ind = f'{ind}_{ind_type}'
        fig = get_map(region_data, contours, indicator=ind, indicator_lab=ind_lab,
                       date=date, selection=map_selection)
        return fig

    @app.callback(Output('lineplot', 'figure'),
                  Input('radio_choice', 'value'),
                  Input('radio_choice_plot', 'value'),
                  Input('map', 'clickData'))
    def update_lineplot(indicator, indicator_type, map_selection):
        """Update lineplot on indicator selection"""
        indicator = f'{indicator}_{indicator_type}'
        return get_lineplot_ovw(region_data, fr_data, indicator=indicator, selection=map_selection)

    @app.callback(Output('radio_choice_plot', 'options'),
                  Output('radio_choice_plot', 'value'),
                  Input('radio_choice', 'value'))
    def update_lineplot_radio(indicator):
        options = [dict(label='Nominal', value='nominal'),
                   dict(label='Rate', value='rate')]
        if indicator == 'rea':
            options[1]['disabled'] = True
        value = 'nominal'
        return options, value

    @app.callback(Output('ind_cards', 'children'),
                  Input('date_picker', 'date'))
    def ind_cards(date):
        # inds = {k: covid_indicators[k] for k in list(covid_indicators.keys())[:2]}
        return create_indicator_card(date, covid_indicators, fcst_data)

    app.layout = html.Div([dcc.Location(id='url'), header, content])

    @app.callback(Output('indicator-table', 'data'),
                  Output('indicator-table', 'columns'),
                  Input('radio_choice_plot', 'value'),
                  Input('radio_choice', 'value'),
                  Input('date_picker', 'date'))
    def fill_table(ind_type, ind, date):
        ind_col = f'{ind}_{ind_type}'
        ind_lab = f'{other_indicators[ind]} ({other_indicators_type[ind_type]})'
        date = date.split('T')[0]
        # filter and format data
        data = region_data[region_data.jour == date]
        data = data[['reg', 'nom_région', ind_col]]\
            .rename(columns={'nom_région': 'Region', 'reg': 'Region Code', ind_col: ind_lab})
        columns = [{'name': colname, 'id': colname} for colname in data.columns]
        return data.to_dict('records'), columns

    @app.callback(Output('table-header', 'children'),
                  Input('date_picker', 'date'))
    def set_table_header(date):
        date = date.split('T')[0]
        return html.B(f"Status by region - {date}")

    @app.callback(Output('forecast_cards', 'children'),
                  Input('exo-vars', 'value'),
                  Input('model_choice', 'value'))
    def forecast_cards(exo_vars, model):
        plt_cards = []
        exo_vars_str = '_'.join(sorted(exo_vars)) if exo_vars else 'no_exo'
        for ind_col, ind in series_to_forecast.items():
            plot = dcc.Graph(figure=get_lineplot_fcst(fcst_data, indicator=ind_col, exo_vars_str=exo_vars_str),
                             style={'width': '100%', 'margin-top': '1vh'})
            plt_cards.append(
                    dbc.Card(
                        dbc.CardBody([
                            html.H5(ind, className='card-title'),
                            plot,
                            html.Hr(className='d-flex my-1', style={'border': '2px gray solid'}),
                            html.B('Residual analysis & Model summary',
                                   className='d-flex display-6', style={'fontSize': 15}),
                            html.Img(src=app.get_asset_url('residuals_%s_%s.png' % (ind_col, exo_vars_str)),
                                     className='d-flex', style={'height': '500px', 'margin-top': '2vh'}),
                            html.Img(src=app.get_asset_url('summary_%s_%s.png' % (ind_col, exo_vars_str)),
                                     className='d-flex', style={'height': '500px'})
                        ]),
                    style={'width': '100%', 'margin-top': '2vh', 'border': '2px #874718 solid', 'align-items': 'center'})
            )

        return plt_cards

    # @app.callback(Output('exo-vars', 'options'),
    #               Input('exo-vars', 'value'))
    # def update_exo_checklist(exo):
    #     options = [{'label': lab, 'value': val, 'disabled': False} for val, lab in exo_variables.items()]
    #     if any([e.startswith('R') for e in exo]):
    #         options = [{'label': lab, 'value': val, 'disabled': val.startswith('R')} for val, lab in exo_variables.items()]
    #     return options

    return app


def create_indicator_card(date, covid_indicators, fcst_data):
    date = date.split('T')[0]
    date = datetime.strptime(date, '%Y-%m-%d')
    ind_cards = []
    for ind, col in covid_indicators.items():
        ind_current = fcst_data.loc[fcst_data.date == date, col]
        ind_previous = fcst_data.loc[fcst_data.date == date - timedelta(days=1), col]
        if ind_current.shape[0] != 0:
            if ind_previous.shape[0] != 0:
                ind_delta = ind_current.values[0] - ind_previous.values[0]
            else:
                ind_delta = ind_current.values[0]
        else:
            ind_cards.append(dbc.Card(
                dbc.CardBody([html.H5(ind, className='card-title'),
                              html.P('', className='card-text')]),
                color='#ffffff'))
            continue

        if ind_delta <= 0:
            color = 'success' if ind_delta < 0 else 'warning'
            sign = ''
        else:
            color = 'danger'
            sign = '+'
        ind_cards.append(dbc.Card(
            dbc.CardBody([html.H5(ind, className='card-title'),
                          html.P(f'{fcst_data.loc[ind_current.index, col].values[0]} ({sign}{ind_delta})',
                                 className='card-text', style={'color': '#ffffff'})]),
            inverse=False,
            color=color))
    return ind_cards


# if __name__ == '__main__':
#     app = covid19_app()
#     app.run_server(debug=True)