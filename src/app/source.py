from onda.pipeline.scrapper import ONDAScrapper

incidence_rate_reg = dict(logical_path='covid19/france/taux_incidence_region_cl_age.csv',
                          url="https://www.data.gouv.fr/fr/datasets/r/ad09241e-52fa-4be8-8298-e5760b43cae2")
incidence_rate_fr = dict(logical_path='covid19/france/taux_incidence.csv',
                         url="https://www.data.gouv.fr/fr/datasets/r/57d44bd6-c9fd-424f-9a72-7834454f9e3c")
incidence_intensive_care_reg = dict(logical_path='covid19/france/incidence_rea_reg_cl_age.csv',
                                    url="https://www.data.gouv.fr/fr/datasets/r/08c18e08-6780-452d-9b8c-ae244ad529b3")
vaccined_regional = dict(logical_path='covid19/france/vaccination_regions.csv',
                         url='https://www.data.gouv.fr/en/datasets/r/eb672d49-7cc7-4114-a5a1-fa6fd147406b')
pandemic_indicators = dict(logical_path='covid19/france/indicateurs_r0.csv',
                         url='https://www.data.gouv.fr/en/datasets/r/381a9472-ce83-407d-9a64-1b8c23af83df')
synthesis_fr = dict(logical_path='covid19/france/synthese.csv',
                    url='https://www.data.gouv.fr/en/datasets/r/d3a98a30-893f-47f7-96c5-2f4bcaaa0d71')
regions_metadata = dict(logical_path='covid19/france/utils/regions_metadata.csv')
geo_contours = dict(logical_path='covid19/france/utils/contours-geographiques-des-nouvelles-regions-metropole.geojson')


# covid19 data sources URLs

chiffres_cles = dict(logical_path='covid19/france/chiffres_cles.csv',
                     url='https://www.data.gouv.fr/en/datasets/r/0b66ca39-1623-4d9c-83ad-5434b7f9e2a4')

hospital_data_fr = dict(logical_path='covid19/france/donnees_hospitalieres.csv',
                        url='https://www.data.gouv.fr/en/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7')

hospital_data_by_age_fr = dict(logical_path='covid19/france/donnees_hospitalieres_classe_d_age.csv',
                               url='https://www.data.gouv.fr/en/datasets/r/08c18e08-6780-452d-9b8c-ae244ad529b3')

patients_share_intensive_care_metropol_fr = dict(logical_path='covid19/france/part_des_patients_rea_metropol.csv',
                                                 url='https://www.data.gouv.fr/fr/datasets/r/62ec32ae-6b3e-4e4a-b81f-eeb4e8759a4d')

patients_share_intensive_care_dep_fr = dict(logical_path='covid19/france/part_des_patients_rea_dep.csv',
                                            url='https://www.data.gouv.fr/fr/datasets/r/5cc79ccf-3df8-4e48-a41c-28839a1498d2')

daily_positivity_rate_dep_fr = dict(logical_path='covid19/france/taux_de_positivite_quotidien_dep.csv',
                                    url='https://www.data.gouv.fr/fr/datasets/r/406c6a23-e283-4300-9484-54e78c8ae675')

daily_positivity_rate_reg_fr = dict(logical_path='covid19/france/taux_de_positivite_quotidien_reg.csv',
                                    url='https://www.data.gouv.fr/fr/datasets/r/001aca18-df6a-45c8-89e6-f82d689e6c01')

daily_positivity_rate_tot_fr = dict(logical_path='covid19/france/taux_de_positivite_quotidien_tot.csv',
                                    url='https://www.data.gouv.fr/fr/datasets/r/dd0de5d9-b5a5-4503-930a-7b08dc0adc7c')

# ONDA Datasource
onda_datasource = ONDAScrapper(sources=[synthesis_fr, incidence_rate_reg, incidence_rate_fr,
                                        incidence_intensive_care_reg,
                                        vaccined_regional, pandemic_indicators])
# synthesis_fr, hospital_data_fr, hospital_data_by_age_fr,
#                                      patients_share_intensive_care_dep_fr, patients_share_intensive_care_metropol_fr,
#                                      daily_positivity_rate_dep_fr, daily_positivity_rate_reg_fr,
#                                      daily_positivity_rate_tot_fr

