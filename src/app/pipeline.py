from app import features
from app.source import onda_datasource, incidence_rate_reg, incidence_rate_fr, incidence_intensive_care_reg,\
    synthesis_fr, vaccined_regional, regions_metadata, geo_contours, pandemic_indicators
from onda.pipeline.onda.publisher import OndaPipelinePublisher, CronPipelineScheduler, EventPipelineScheduler


pipe = OndaPipelinePublisher(default_storage='s3')
pipe.add(*onda_datasource.ingestion_jobs())
pipe.discover(features)
# demander si je veux pas scheduler : f"/{geo_contours['logical_path']}", f"/{regions_metadata['logical_path']}"
pipe.schedule(CronPipelineScheduler([f"/{incidence_rate_reg['logical_path']}",
                                     f"/{incidence_rate_fr['logical_path']}",
                                     f"/{incidence_intensive_care_reg['logical_path']}",
                                     f"/{vaccined_regional['logical_path']}"], '*/360 * * * *'))
pipe.schedule(EventPipelineScheduler(['/covid19/france/consolidation_region.csv',
                                      '/covid19/france/consolidation_fr.csv']))
pipe.schedule(CronPipelineScheduler([f"/{synthesis_fr['logical_path']}"], '*/360 * * * *'))
pipe.schedule(CronPipelineScheduler([f"/{pandemic_indicators['logical_path']}"], '*/360 * * * *'))
pipe.schedule(EventPipelineScheduler(['/covid19/france/forecasts.csv']))
pipe.schedule(EventPipelineScheduler(["/test_dash_app"]))


