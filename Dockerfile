FROM registry.gitlab.com/jbmevel/flum_aesi/jenga

WORKDIR /app

COPY requirements.txt ./requirements-app.txt
RUN pip install --no-cache-dir -r ./requirements-app.txt

COPY ./src/app/ ./app
CMD ["python", "-um", "onda.main", "-ervvp", "app.pipeline:pipe"]
